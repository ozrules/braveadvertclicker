import time
import json
import random
import pyautogui


def work(sites=[], dictionary={}):
    start = time.time()

    time.sleep(10)

    try:
        while True:
            # ------------------------------------------------------------------
            # go to website to show usage of browser
            pyautogui.click(x=665, y=47)
            time.sleep(random.randint(1, 5))
            pyautogui.click(x=665, y=47)
            time.sleep(random.randint(1, 5))
            pyautogui.hotkey("ctrl", "a")
            time.sleep(random.randint(1, 5))
            pyautogui.write(random.choice(sites))
            time.sleep(random.randint(1, 5))
            pyautogui.press("enter")
            #
            time.sleep(random.randint(5, 10))
            # ------------------------------------------------------------------

            # ------------------------------------------------------------------
            # search term for PRESEARCH
            # pyautogui.click(x=665, y=47)
            # time.sleep(random.randint(1, 5))
            # pyautogui.click(x=665, y=47)
            # time.sleep(random.randint(1, 5))
            # pyautogui.hotkey("ctrl", "a")
            # time.sleep(random.randint(1, 5))
            # pyautogui.write(random.choice(list(d.keys())))
            # time.sleep(random.randint(1, 5))
            # pyautogui.press("enter")
            # #
            # time.sleep(random.randint(1, 5))
            # ------------------------------------------------------------------

            # ------------------------------------------------------------------
            # click ad
            # Point(x=1312, y=51)
            pyautogui.click(x=1312, y=51)
            time.sleep(1)
            pyautogui.click(x=1312, y=51)
            #
            time.sleep(random.randint(1, 5))
            # ------------------------------------------------------------------

            # ------------------------------------------------------------------
            # right click close tabs to the right
            pyautogui.rightClick(x=265, y=15)
            time.sleep(random.randint(1,5))
            pyautogui.click(x=293, y=267)
            # ------------------------------------------------------------------

            # sleep and do it again
            time.sleep(random.randint(10 , 30))

            # elapsed time
            elapsed = time.time() - start
            print(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
            print(elapsed % 3600)

    except KeyboardInterrupt:
        exit()
    except Exception as err:
        print(err)



TOPSITES = [
    "youtube.com",
    "en.wikipedia.org",
    "twitter.com",
    "facebook.com",
    "amazon.com",
    "yelp.com",
    "reddit.com",
    "imdb.com",
    "fandom.com",
    "pinterest.com",
    "tripadvisor.com",
    "instagram.com",
    "walmart.com",
    "craigslist.org",
    "ebay.com",
    "linkedin.com",
    "play.google.com",
    "healthline.com",
    "etsy.com",
    "indeed.com",
    "apple.com",
    "espn.com",
    "webmd.com",
    "fb.com",
    "nytimes.com",
    "google.com",
    "cnn.com",
    "merriam-webster.com",
    "gamepedia.com",
    "microsoft.com",
    "target.com",
    "homedepot.com",
    "quora.com",
    "nih.gov",
    "rottentomatoes.com",
    "netflix.com",
    "quizlet.com",
    "weather.com",
    "mapquest.com",
    "britannica.com",
    "businessinsider.com",
    "dictionary.com",
    "zillow.com",
    "mayoclinic.org",
    "bestbuy.com",
    "theguardian.com",
    "yahoo.com",
    "msn.com",
    "usatoday.com",
    "medicalnewstoday.com",
    "urbandictionary.com",
    "usnews.com",
    "foxnews.com",
    "genius.com",
    "allrecipes.com",
    "spotify.com",
    "glassdoor.com",
    "forbes.com",
    "cnet.com",
    "finance.yahoo.com",
    "irs.gov",
    "lowes.com",
    "mail.yahoo.com",
    "aol.com",
    "steampowered.com",
    "washingtonpost.com",
    "usps.com",
    "office.com",
    "retailmenot.com",
    "wiktionary.org",
    "paypal.com",
    "foodnetwork.com",
    "hulu.com",
    "live.com",
    "cbssports.com",
    "wayfair.com",
    "ca.gov",
    "bleacherreport.com",
    "macys.com",
    "accuweather.com",
    "xfinity.com",
    "go.com",
    "techradar.com",
    "groupon.com",
    "investopedia.com",
    "yellowpages.com",
    "steamcommunity.com",
    "chase.com",
    "wellsfargo.com",
    "npr.org",
    "apartments.com",
    "roblox.com",
    "huffpost.com",
    "books.google.com",
    "bankofamerica.com",
    "bbb.org",
    "expedia.com",
    "wikihow.com",
    "ign.com",
    "wowhead.com"
]



if __name__ == "__main__":
    d = {}
    with open("dictionary.json") as f:
        d = json.load(f)

    work(sites=TOPSITES, dictionary=d)